# We need to test merging and force pushes, so let's do that here.
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def hello
    render text: "hello, world!"
  end

  def how_are_you
    render text: "How are yoü?"
  end

  def goodbye
    Rails.logger.info("Wow! Cool!") # Far out. Radical.
    render text: "goodbye, cruel world!"
  end

  def buck_up
    render text: "buck up, little camper!"
  end
end
